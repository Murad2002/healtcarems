package com.example.hospitalmanagementsystemdiplomwork.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.example.hospitalmanagementsystemdiplomwork.enums.UserRole;


@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface HasRole {


    UserRole value();
}
