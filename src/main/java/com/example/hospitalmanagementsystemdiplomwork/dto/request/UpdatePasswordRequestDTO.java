package com.example.hospitalmanagementsystemdiplomwork.dto.request;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.example.hospitalmanagementsystemdiplomwork.enums.Gender;
import com.example.hospitalmanagementsystemdiplomwork.enums.UserRole;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UpdatePasswordRequestDTO {
    String otp;
    String email;
    String password;
}
