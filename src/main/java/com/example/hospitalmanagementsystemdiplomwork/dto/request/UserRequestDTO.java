package com.example.hospitalmanagementsystemdiplomwork.dto.request;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.example.hospitalmanagementsystemdiplomwork.enums.Gender;
import com.example.hospitalmanagementsystemdiplomwork.enums.UserRole;
import com.example.hospitalmanagementsystemdiplomwork.enums.UserStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserRequestDTO {
     String name;
     String surName;
     @JsonFormat(pattern = "yyyy-MM-dd")
     LocalDate birthDate;
     String phoneNumber;
     String email;
     Gender gender;
     String address;
     UserRole role;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
     LocalDateTime registrationDate;
     String password;
}
