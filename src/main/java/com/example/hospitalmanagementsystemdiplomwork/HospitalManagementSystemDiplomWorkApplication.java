package com.example.hospitalmanagementsystemdiplomwork;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HospitalManagementSystemDiplomWorkApplication {

    public static void main(String[] args) {
        SpringApplication.run(HospitalManagementSystemDiplomWorkApplication.class, args);
    }

}
