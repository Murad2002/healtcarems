package com.example.hospitalmanagementsystemdiplomwork.config.security;

import java.util.Optional;

import com.example.hospitalmanagementsystemdiplomwork.util.JwtUtil;
import io.jsonwebtoken.Claims;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;

import static com.example.hospitalmanagementsystemdiplomwork.constants.CommonConstants.ACCESS_TOKEN;
import static com.example.hospitalmanagementsystemdiplomwork.constants.CommonConstants.REFRESH_TOKEN;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@Configuration
@Slf4j
@RequiredArgsConstructor
public class TokenService {
    private static final String BEARER = "Bearer ";
    private final JwtUtil jwtUtil;

    public Optional<Authentication> getAuthentication(HttpServletRequest request) {
        log.info("Token Service");
        String authorization = request.getHeader(AUTHORIZATION);
        StringBuilder token = new StringBuilder();
        if (authorization != null && authorization.startsWith(BEARER)) {
            token.append(authorization.substring(BEARER.length()));
            Claims claims = jwtUtil.parseToken(token.toString());
            String type = claims.get("type", String.class);

            if (!request.getRequestURL().toString().endsWith("issue-token")) {
                if (type.equals(REFRESH_TOKEN)) {
                    return Optional.empty();
                }
            } else {
                if (type.equals(ACCESS_TOKEN)) {
                    return Optional.empty();
                }
            }
            Authentication authentication = jwtUtil.getAuthenticationBearer(claims);
            return Optional.of(authentication);
        }
        return Optional.empty();
    }
}
