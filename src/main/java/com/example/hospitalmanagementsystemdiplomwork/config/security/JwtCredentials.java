package com.example.hospitalmanagementsystemdiplomwork.config.security;

import com.example.hospitalmanagementsystemdiplomwork.enums.UserRole;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class JwtCredentials {

    Long iat;
    String email;
    String type;
    Integer userId;
    Long exp;
    UserRole role;


}
