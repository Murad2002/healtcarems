package com.example.hospitalmanagementsystemdiplomwork.config.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;

@Configuration
@RequiredArgsConstructor
public class SecurityService {

    private final ObjectMapper objectMapper;

    public JwtCredentials getCurrentJwtCredentials() {
        var securityContext = SecurityContextHolder.getContext();
        var authentication = securityContext.getAuthentication();
        return objectMapper.convertValue(authentication.getPrincipal(), JwtCredentials.class);
    }
}
