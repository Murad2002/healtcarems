package com.example.hospitalmanagementsystemdiplomwork.config.security;


import java.io.IOException;
import java.time.OffsetDateTime;
import java.util.Optional;

import com.example.hospitalmanagementsystemdiplomwork.exception.ErrorResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.security.SignatureException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import static com.example.hospitalmanagementsystemdiplomwork.enums.ErrorCodes.JWT_EXPIRED;
import static com.example.hospitalmanagementsystemdiplomwork.enums.ErrorCodes.JWT_MALFORMED;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
@RequiredArgsConstructor
@Configuration
public class AuthRequestFilter extends OncePerRequestFilter {

    private final TokenService tokenService;
    private final ObjectMapper objectMapper;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        log.info("Filter request against auth services {}", tokenService);
        try {
            Optional<Authentication> authOptional = Optional.empty();
            authOptional = authOptional.or(() -> tokenService.getAuthentication(request));
            authOptional.ifPresent(
                    authentication -> SecurityContextHolder.getContext().setAuthentication(authentication));
            filterChain.doFilter(request, response);
        } catch (ExpiredJwtException | MalformedJwtException | SignatureException ex) {
            setExceptionResponse(ex, request, response);
        }
    }


    private void setExceptionResponse(Exception ex, HttpServletRequest request, HttpServletResponse response) throws IOException {
        log.info("Error logging in : {}", ex.getMessage());
        if (ex instanceof ExpiredJwtException) {
            response.setStatus(650);
            ErrorResponse errorResponse = ErrorResponse.builder()
                    .timestamp(OffsetDateTime.now())
                    .status(650)
                    .code(JWT_EXPIRED.code)
                    .message("EXPIRED")
                    .detail("EXPIRED")
                    .path(request.getRequestURL().toString())
                    .build();
            response.setContentType(APPLICATION_JSON_VALUE);
            objectMapper.writeValue(response.getOutputStream(), errorResponse);
        } else if (ex instanceof MalformedJwtException) {
            response.setStatus(649);
            ErrorResponse errorResponse = com.example.hospitalmanagementsystemdiplomwork.exception.ErrorResponse.builder()
                    .timestamp(OffsetDateTime.now())
                    .status(649)
                    .code(JWT_MALFORMED.code)
                    .message("MALFORMED")
                    .detail("MALFORMED")
                    .path(request.getRequestURL().toString())
                    .build();
            response.setContentType(APPLICATION_JSON_VALUE);
            objectMapper.writeValue(response.getOutputStream(), errorResponse);
        } else if (ex instanceof SignatureException) {
            response.setStatus(648);
            ErrorResponse errorResponse = ErrorResponse.builder()
                    .timestamp(OffsetDateTime.now())
                    .status(648)
                    .code("JWT-SIGNATURE-PROBLEM")
                    .message("SIGNATURE")
                    .detail("SIGNATURE")
                    .path(request.getRequestURL().toString())
                    .build();
            response.setContentType(APPLICATION_JSON_VALUE);
            objectMapper.writeValue(response.getOutputStream(), errorResponse);
        }

    }
}
