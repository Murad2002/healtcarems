package com.example.hospitalmanagementsystemdiplomwork.enums;


import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Gender {
    MALE(1),
    FEMALE(0);

    private final int code;
}
