package com.example.hospitalmanagementsystemdiplomwork.enums;

public enum UserStatus {
    ACTIVE,
    NOT_ACTIVATED,
    IN_ACTIVE;

}
