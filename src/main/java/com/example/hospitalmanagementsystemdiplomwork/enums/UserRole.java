package com.example.hospitalmanagementsystemdiplomwork.enums;

public enum UserRole {
    ROLE_PATIENT,
    ROLE_DOCTOR,
    ROLE_RECEPTIONIST
}
