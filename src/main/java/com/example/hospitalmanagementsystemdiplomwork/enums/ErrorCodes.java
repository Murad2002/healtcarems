package com.example.hospitalmanagementsystemdiplomwork.enums;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ErrorCodes {

    USER_NOT_FOUND("USER-NOT-FOUND"),
    JWT_EXPIRED("JWT-EXPIRED"),

    JWT_MALFORMED("JWT-MALFORMED");

    public final String code;
}
