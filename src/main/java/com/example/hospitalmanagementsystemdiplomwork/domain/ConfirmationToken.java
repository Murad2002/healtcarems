package com.example.hospitalmanagementsystemdiplomwork.domain;

import java.time.LocalDateTime;
import java.util.UUID;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.CreationTimestamp;

@Data
@Entity(name = "CONFIRMATION_TOKENS")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ConfirmationToken {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "CONFIRMATION_TOKEN")
    String confirmationToken;

    @OneToOne
    @JoinColumn(nullable = false, name = "id")
    User user;

    @Column(name = "INSERTDATE")
    @CreationTimestamp
    LocalDateTime insertDate;

    public ConfirmationToken(User user) {
        confirmationToken = UUID.randomUUID().toString();
    }
}
