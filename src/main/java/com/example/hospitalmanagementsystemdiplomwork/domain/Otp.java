package com.example.hospitalmanagementsystemdiplomwork.domain;

import java.time.LocalDateTime;

import com.example.hospitalmanagementsystemdiplomwork.enums.OtpStatus;
import com.example.hospitalmanagementsystemdiplomwork.enums.UserStatus;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "otp_users")
public class Otp {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String otp;
    private int useCount;
    @ManyToOne
    private User user;
    @Enumerated(EnumType.STRING)
    private OtpStatus otpStatus;
    @CreationTimestamp
    private LocalDateTime createDate;

}
