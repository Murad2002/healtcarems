package com.example.hospitalmanagementsystemdiplomwork.domain;

import java.time.LocalDate;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "appointments")
public class Appointment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private User doctor;
    @OneToOne
    private Department department;
    @ManyToOne
    private User patient;
    private String name;
    private String surname;
    private String phoneNumber;
    private String email;
    private Double price;
    private LocalDate appointmentDay;
    @ManyToOne
    private AppointmentTime time;


}
