package com.example.hospitalmanagementsystemdiplomwork.domain;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

import com.example.hospitalmanagementsystemdiplomwork.enums.Gender;
import com.example.hospitalmanagementsystemdiplomwork.enums.UserStatus;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String surName;
    private String title;
    LocalDate birthDate;
    private String phoneNumber;
    private String email;
    @Enumerated(EnumType.STRING)
    private Gender gender;
    private String address;
    @Enumerated(EnumType.STRING)
    private UserStatus userStatus = UserStatus.NOT_ACTIVATED;
    @CreationTimestamp
    private LocalDateTime createDate;
    private String password;
    @OneToOne(cascade = CascadeType.ALL)
    private Authority authority;

    @ManyToOne
    private Department department;
    @OneToMany(mappedBy = "doctor")
    private Set<Appointment> appointmentSet;

    @OneToMany(mappedBy = "patient")
    private Set<Prescription> prescriptionSet;
    @OneToMany(mappedBy = "patient")
    private Set<Bill> billSet;
    @OneToMany(mappedBy = "patient")
    private Set<Application> applicationSet;
}
