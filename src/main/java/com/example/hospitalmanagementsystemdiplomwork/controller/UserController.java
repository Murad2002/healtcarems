package com.example.hospitalmanagementsystemdiplomwork.controller;

import com.example.hospitalmanagementsystemdiplomwork.dto.request.UpdatePasswordRequestDTO;
import com.example.hospitalmanagementsystemdiplomwork.dto.request.UserRequestDTO;
import com.example.hospitalmanagementsystemdiplomwork.dto.response.UserResponseDTO;
import com.example.hospitalmanagementsystemdiplomwork.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserController {
    private final UserService userService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void signUpUser(@RequestBody UserRequestDTO userRequestDTO) {
        userService.createUser(userRequestDTO);
    }

    @PostMapping("/login")
    @ResponseStatus(HttpStatus.OK)
    public UserResponseDTO loginUser(@RequestBody UserRequestDTO userRequestDTO) {
        return userService.checkUserCredentialsAndReturnToken(userRequestDTO);
    }

    @PostMapping("/issue-token")
    @ResponseStatus(HttpStatus.OK)
    public UserResponseDTO getNewToken() {
        return userService.getNewToken();
    }

    @PostMapping("/send-code/{email}")
    @ResponseStatus(HttpStatus.OK)
    public void sendCode(@PathVariable String email) {
        userService.sendCode(email);
    }

    @PostMapping("/update-password")
    @ResponseStatus(HttpStatus.OK)
    public void updatePassword(@RequestBody UpdatePasswordRequestDTO dto) {
        userService.updatePassword(dto);
    }

    @GetMapping(value = "/activate-account")
    public ModelAndView confirmUserAccount(ModelAndView modelAndView, @RequestParam("token") String confirmationToken) {
        return userService.confirmUserAccount(modelAndView, confirmationToken);
    }

}
