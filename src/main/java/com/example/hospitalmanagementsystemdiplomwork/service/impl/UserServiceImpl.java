package com.example.hospitalmanagementsystemdiplomwork.service.impl;

import java.time.LocalDateTime;
import java.util.Random;

import com.example.hospitalmanagementsystemdiplomwork.config.security.JwtCredentials;
import com.example.hospitalmanagementsystemdiplomwork.config.security.SecurityService;
import com.example.hospitalmanagementsystemdiplomwork.domain.Authority;
import com.example.hospitalmanagementsystemdiplomwork.domain.ConfirmationToken;
import com.example.hospitalmanagementsystemdiplomwork.domain.Otp;
import com.example.hospitalmanagementsystemdiplomwork.domain.User;
import com.example.hospitalmanagementsystemdiplomwork.dto.request.UpdatePasswordRequestDTO;
import com.example.hospitalmanagementsystemdiplomwork.dto.request.UserRequestDTO;
import com.example.hospitalmanagementsystemdiplomwork.dto.response.UserResponseDTO;
import com.example.hospitalmanagementsystemdiplomwork.enums.OtpStatus;
import com.example.hospitalmanagementsystemdiplomwork.enums.UserStatus;
import com.example.hospitalmanagementsystemdiplomwork.repository.ConfirmationTokenRepository;
import com.example.hospitalmanagementsystemdiplomwork.repository.OtpRepository;
import com.example.hospitalmanagementsystemdiplomwork.repository.UserRepository;
import com.example.hospitalmanagementsystemdiplomwork.service.UserService;
import com.example.hospitalmanagementsystemdiplomwork.util.JwtUtil;
import com.example.hospitalmanagementsystemdiplomwork.util.MailUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;

import static com.example.hospitalmanagementsystemdiplomwork.constants.CommonConstants.ACCESS_TOKEN;
import static com.example.hospitalmanagementsystemdiplomwork.constants.CommonConstants.REFRESH_TOKEN;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final ModelMapper modelMapper;
    private final UserRepository userRepository;
    private final ConfirmationTokenRepository confirmationTokenRepository;
    private final PasswordEncoder passwordEncoder;
    private final MailUtil mailUtil;
    private final JwtUtil jwtUtil;
    private final SecurityService securityService;
    private final OtpRepository otpRepository;
    private final Random rand = new Random();

    @Transactional
    @Override
    public void createUser(UserRequestDTO userRequestDTO) {
        if (userRepository.findByEmailAndUserStatusNot(userRequestDTO.getEmail(), UserStatus.IN_ACTIVE).isPresent()) {
            throw new RuntimeException("User Email Already Registered");
        }
        User user = modelMapper.map(userRequestDTO, User.class);
        user.setPassword(passwordEncoder.encode(userRequestDTO.getPassword()));
        Authority authority = new Authority();
        authority.setRole(userRequestDTO.getRole());
        user.setAuthority(authority);
        userRepository.save(user);
        ConfirmationToken confirmationToken = new ConfirmationToken(user);
        String text = "To confirm your account, please click here : "
                + "http://localhost:8080/v1/user/activate-account?token=" + confirmationToken.getConfirmationToken()
                + "\nLink Will be Expired in 20 seconds";
        Thread thread = new Thread(() ->
                mailUtil.sendEmail(user.getEmail(), "Complete Registration!", "SMART-MED-HOSPITAL", text));
        thread.start();
        confirmationTokenRepository.save(confirmationToken);
    }

    @Override
    public ModelAndView confirmUserAccount(ModelAndView modelAndView, String confirmationToken) {
        try {
            var token = confirmationTokenRepository.findByConfirmationToken(confirmationToken);
            if (token.isPresent()) {
                User user = userRepository.findByEmail(token.get().getUser().getEmail()).orElseThrow();
                if (user.getCreateDate().plusSeconds(20).isBefore(LocalDateTime.now())) {
                    if (user.getUserStatus() == UserStatus.NOT_ACTIVATED) {
                        user.setUserStatus(UserStatus.IN_ACTIVE);
                        userRepository.save(user);
                    }
                    log.info("EXPIRED");
                    modelAndView.setViewName("expired.html");
                    return modelAndView;
                }
                user.setUserStatus(UserStatus.ACTIVE);
                userRepository.save(user);
                modelAndView.setViewName("accountVerified.html");
                modelAndView.addObject("email", token.get().getUser().getEmail());
            } else {
                modelAndView.setViewName("error.html");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return modelAndView;
    }

    public UserResponseDTO checkUserCredentialsAndReturnToken(UserRequestDTO reqDto) {
        var user = userRepository.findByEmailAndUserStatus(reqDto.getEmail(), UserStatus.ACTIVE).orElseThrow();
        if (passwordEncoder
                .matches(reqDto.getPassword(), user.getPassword()))
            return getUserRespDTO(user);
        throw new RuntimeException("Bad credentials !");
    }

    public UserResponseDTO getNewToken() {
        JwtCredentials credentials = securityService.getCurrentJwtCredentials();
        User user = userRepository.findByEmailAndUserStatus(credentials.getEmail(), UserStatus.ACTIVE).orElseThrow();
        return getUserRespDTO(user);
    }

    @Override
    public void sendCode(String email) {
        var user = userRepository.findByEmailAndUserStatus(email, UserStatus.ACTIVE).orElseThrow(() -> new RuntimeException("Invalid Email"));
        String otp = (rand.nextInt(900) + 100) + "-" + (rand.nextInt(900) + 100);
        otpRepository.save(Otp.builder()
                .user(user)
                .otp(otp)
                .otpStatus(OtpStatus.UN_USED)
                .useCount(0)
                .build());
        Thread thread = new Thread(() ->
                mailUtil.sendEmail(user.getEmail(), "OTP Code", "SMART-MED-HOSPITAL", "Otp : " + otp));
        thread.start();
    }

    @Override
    public void updatePassword(UpdatePasswordRequestDTO dto) {
        var user = userRepository.findByEmailAndUserStatusNot
                (dto.getEmail(), UserStatus.IN_ACTIVE).orElseThrow(() -> new RuntimeException("Invalid Email"));
        var otp = otpRepository.findByUserAndOtp(user, dto.getOtp()).orElseThrow(
                () -> new RuntimeException("Wrong otp !"));
        if (otp.getCreateDate().plusSeconds(180).isBefore(LocalDateTime.now())) {
            throw new RuntimeException("OTP Expired");
        }
        if (otp.getUseCount() == 1) {
            throw new RuntimeException("OTP Already Used !");
        }
        otp.setUseCount(1);
        otp.setOtpStatus(OtpStatus.USED);
        otpRepository.save(otp);
        user.setPassword(passwordEncoder.encode(dto.getPassword()));
        userRepository.save(user);
    }

    private UserResponseDTO getUserRespDTO(User user) {
        return UserResponseDTO.builder()
                .accessToken(jwtUtil.issueToken(user, 3, ACCESS_TOKEN))
                .refreshToken(jwtUtil.issueToken(user, 5, REFRESH_TOKEN))
                .build();
    }
}
