package com.example.hospitalmanagementsystemdiplomwork.service;

import com.example.hospitalmanagementsystemdiplomwork.dto.request.UpdatePasswordRequestDTO;
import com.example.hospitalmanagementsystemdiplomwork.dto.request.UserRequestDTO;
import com.example.hospitalmanagementsystemdiplomwork.dto.response.UserResponseDTO;
import org.springframework.web.servlet.ModelAndView;

public interface UserService {
    void createUser(UserRequestDTO userRequestDTO);

    ModelAndView confirmUserAccount(ModelAndView modelAndView, String confirmationToken);

    UserResponseDTO checkUserCredentialsAndReturnToken(UserRequestDTO reqDto);

    UserResponseDTO getNewToken();

    void sendCode(String email);

    void updatePassword(UpdatePasswordRequestDTO dto);
}
