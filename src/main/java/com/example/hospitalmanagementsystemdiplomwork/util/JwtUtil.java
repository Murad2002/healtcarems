package com.example.hospitalmanagementsystemdiplomwork.util;

import java.security.Key;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.example.hospitalmanagementsystemdiplomwork.domain.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class JwtUtil {

    private Key key;

    @Value("${jwt.secretKey}")
    private String secretKey;

    @PostConstruct
    public void init() {
        byte[] keyBytes;
        keyBytes = Decoders.BASE64.decode(secretKey);
        key = Keys.hmacShaKeyFor(keyBytes);
    }

    public Claims parseToken(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    public String issueToken(User user, int minute, String type) {
        log.info("Issue JWT token to {}", user);
        final JwtBuilder jwtBuilder = Jwts.builder()
                .setIssuedAt(new Date())
                .claim("email", user.getEmail())
                .claim("userId", user.getId())
                .claim("type",type)
                .claim("role", user.getAuthority().getRole().name())
                .setExpiration(Date.from(Instant.now().plusSeconds(minute * 60)))
                .setHeader(Map.of("type", "JWT"))
                .signWith(key, SignatureAlgorithm.HS256);
        return jwtBuilder.compact();
    }

    public Authentication getAuthenticationBearer(Claims claims) {
        String role = claims.get("role", String.class);
        List<GrantedAuthority> authorityList = List.of(new SimpleGrantedAuthority(role));
        return new UsernamePasswordAuthenticationToken(claims, "", authorityList);
    }


}
