package com.example.hospitalmanagementsystemdiplomwork.util;

import lombok.RequiredArgsConstructor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service("mailService")
public class MailUtil {
    private final JavaMailSender javaMailSender;

    public void sendSimpleMessage(SimpleMailMessage email) {
        javaMailSender.send(email);
    }

    public void sendEmail(String to, String subject,String from , String text){
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(to);
        mailMessage.setSubject(subject);
        mailMessage.setFrom(from);
        mailMessage.setText(text);
        sendSimpleMessage(mailMessage);

    }


}
