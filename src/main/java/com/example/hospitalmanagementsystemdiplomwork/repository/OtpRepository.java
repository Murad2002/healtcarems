package com.example.hospitalmanagementsystemdiplomwork.repository;

import java.util.Optional;

import com.example.hospitalmanagementsystemdiplomwork.domain.Otp;
import com.example.hospitalmanagementsystemdiplomwork.domain.User;
import com.example.hospitalmanagementsystemdiplomwork.enums.OtpStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OtpRepository extends JpaRepository<Otp, Long> {

    Optional<Otp> findByUserAndOtp(User user, String otp);
}
