package com.example.hospitalmanagementsystemdiplomwork.repository;

import java.util.Optional;

import com.example.hospitalmanagementsystemdiplomwork.domain.User;
import com.example.hospitalmanagementsystemdiplomwork.enums.UserStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByEmailAndUserStatus(String email, UserStatus status);

    Optional<User> findByEmailAndUserStatusNot(String email, UserStatus status);

    Optional<User> findByEmail(String userEmail);
}
