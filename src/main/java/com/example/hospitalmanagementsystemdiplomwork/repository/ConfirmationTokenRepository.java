package com.example.hospitalmanagementsystemdiplomwork.repository;

import java.util.Optional;

import com.example.hospitalmanagementsystemdiplomwork.domain.ConfirmationToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConfirmationTokenRepository extends JpaRepository<ConfirmationToken, Long> {

    Optional<ConfirmationToken> findByConfirmationToken(String confirmationToken);
}
