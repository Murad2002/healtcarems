package com.example.hospitalmanagementsystemdiplomwork.constants;

public class CommonConstants {

    public static final String ACCESS_TOKEN = "ACCESS_TOKEN";
    public static final String REFRESH_TOKEN = "REFRESH_TOKEN";

}
