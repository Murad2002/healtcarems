package com.example.hospitalmanagementsystemdiplomwork.aspect;

import com.example.hospitalmanagementsystemdiplomwork.annotation.HasRole;
import com.example.hospitalmanagementsystemdiplomwork.config.security.SecurityService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
@RequiredArgsConstructor
public class RoleConfigAspect {

    private final SecurityService securityService;

    @Around("@annotation(com.example.hospitalmanagementsystemdiplomwork.annotation.HasRole)")
    public Object checkAuthenticationMethod(ProceedingJoinPoint joinPoint) throws Throwable {
        var signature = (MethodSignature) joinPoint.getSignature();
        var annotation = signature.getMethod().getAnnotation(HasRole.class);
        if (securityService.getCurrentJwtCredentials().getRole() != annotation.value()) {
            throw new RuntimeException("INVALID ROLE");
        }
        return joinPoint.proceed();
    }

}
